import jenkins
import jenkinsconfig as cfg

 
class PythonJenkins():
    """
    Python Jenkins is a python wrapper for the Jenkins REST API which aims
    to provide a more conventionally pythonic way of controlling a Jenkins server
    """

    def test(self):
        server = jenkins.Jenkins('http://192.168.99.100:8080/jenkins/', username='admin', password='admin')
        return server


    def logging_jenkins(self):
         server = jenkins.Jenkins(cfg.jenkins['env'], username=cfg.jenkins['user'], password=cfg.jenkins['passwd'])
         return server


    def get_version(self):
        server = self.logging_jenkins()
        return server.get_version()


    def get_jobs(self):
        server = self.logging_jenkins()
        return server.get_all_jobs()


    def build_job(self, job_name):
        #server = self.logging_jenkins()
        #return server.build_job(jon_name)
        pass

    
    def create_job(self):
        #server = self.logging_jenkins()
        #return server.
        pass


if __name__ == "__main__":
    test = PythonJenkins()
    server = test.test()
    info = server.get_info()
    print(info)