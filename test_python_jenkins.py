"""
Unittest
"""

import unittest
from unittest.mock import patch
from python_jenkins import PythonJenkins

#Jenkings API Request
import json
import requests

class TestLoggin(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        #Example: antes de toda prueba, puedes crear un set up incial, alguna DB con datos polulados, etc.
        print('setUpClass')


    @classmethod
    def tearDownClass(cls):
        #Example: Despues de todas las clases puede destruir contenido. Siguiendo el ejemplo se puede destruir la DB.
        print('tearDownClass')


    def setUp(self):
        #crea al principio de cada test los datos/instancias/etc iniciales ejecutar dicho test
        print('setUp')
        #self.test_somenting
        pass


    def tearDown(self):
        #Limpia todos los valores al terminar el Test.
        print('tearDown')
        pass


    def test_loggin_jenkings(self):
        server = self.logging_mock('http://192.168.99.100:8080/jenkins/', username='admin', password='admin')
        jsonObj = json.dumps({'4': 5, '6': 7})
        self.assertEqual(type(server) , type(jsonObj))


    def logging_mock(self, url, username, password):
        response = requests.get(f'http://company.com/jenkins/')
        if response.ok:
            jsonObj = json.dumps({'4': 5, '6': 7})
            return jsonObj
        else:
            return 'ConnectionResetError: [WinError 10054] An existing connection was forcibly closed by the remote host'

if __name__ == '__main__':
    unittest.main()